const commentsControllers = require('../controllers/comments')

const setupCommentsRoutes = (app) => {
  app.post('/comments', commentsControllers.createNewComment)
  app.get('/comments', commentsControllers.findComments)
  app.get('/comments/:commentId', commentsControllers.findComment)
}

module.exports = setupCommentsRoutes
