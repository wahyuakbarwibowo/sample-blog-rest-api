const authControllers = require('../controllers/auth')

const setupAuthRoutes = (app) => {
  app.post('/login', authControllers.login)
}

module.exports = setupAuthRoutes
