const postsControllers = require('../controllers/posts')

const setupPostsRoutes = (app) => {
  app.post('/posts', postsControllers.createNewPost)
  app.get('/posts', postsControllers.findPosts)
  app.get('/posts/:postId', postsControllers.findPost)
}

module.exports = setupPostsRoutes
