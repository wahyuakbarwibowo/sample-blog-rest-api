const setupAuthRoutes = require('./auth')
const setupCommentsRoutes = require('./comments')
const setupPostsRoutes = require('./posts')
const setupRatingsRoutes = require('./ratings')
const setupUsersRoutes = require('./users')

const setupRoutes = (app) => {
  setupAuthRoutes(app)
  setupCommentsRoutes(app)
  setupPostsRoutes(app)
  setupRatingsRoutes(app)
  setupUsersRoutes(app)
}

module.exports = setupRoutes
