const ratingsControllers = require('../controllers/ratings')

const setupRatingsRoutes = (app) => {
  app.post('/ratings', ratingsControllers.createNewRating)
  app.get('/ratings', ratingsControllers.findRatings)
  app.get('/ratings/:ratingId', ratingsControllers.findRating)
}

module.exports = setupRatingsRoutes
