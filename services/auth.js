const bcrypt = require('bcrypt')
const jsonwebtoken = require('jsonwebtoken')

const environment = require('../environment')

const authServices = {
  passwords: {
    comparePasswords: (dbPassword, requestPassword) => {
      return bcrypt.compareSync(requestPassword, dbPassword)
    },
    hashPassword: (requestPassword) => {
      return bcrypt.hashSync(requestPassword, environment.bcryptSalt)
    },
  },
  tokens: {
    getTokenFromUser: (user) => {
      return jsonwebtoken.sign(user, environment.jwtsecret)
    },
    getTokenPayload: (token) => {
      return jsonwebtoken.decode(token, { complete: true, json: true })
    },
    verifyTokenValidity: (token) => {
      return jsonwebtoken.verify(token, environment.jwtsecret)
    },
  },
}

module.exports = authServices
