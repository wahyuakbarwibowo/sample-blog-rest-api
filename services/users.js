const prisma = require("../prisma/client")
const superstruct = require('superstruct')

const authServices = require('./auth')

const schemasForUsers = {
  createPayload: superstruct.object({
    username: superstruct.string(),
    password: superstruct.string(),
  }),
  updatePayload: superstruct.optional({
    content: superstruct.string(),
  }),
}

const usersServices = {
  createNewUser: (createPayload) => {
    superstruct.assert(payload, schemasForUsers.createPayload)
    return prisma.user.create({
      data: {
        username: createPayload.username,
        hash: authServices.passwords.hashPassword(createPayload.password),
      },
    })
  },
  findUser: (userId) => {
    return prisma.user.findUnique({ where: { user_id: userId } })
  },
  findUserByUsername: (userName) => {
    return prisma.user.findUnique({ where: { username: userName } })
  },
  findUsers: () => {
    return prisma.user.findMany({})
  },
  updateUser: (userId, updatePayload) => {
    superstruct.assert(updatePayload, schemasForUsers.updatePayload)
    return prisma.user.update({
      where: {
        user_id: userId,
      },
      data: updatePayload,
    })
  },
}

module.exports = usersServices
