const dotenv = require('dotenv')

dotenv.config()

const environment = {
  bcryptSalt: process.env.BCRYPT_SALT || 'salt',
  httpPort: process.env.REST_API_HTTP_PORT || 5001,
  jwtsecret: process.env.JWT_SECRET || 'secret',
}

module.exports = environment
