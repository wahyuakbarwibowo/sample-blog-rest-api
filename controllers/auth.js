const superstruct = require('superstruct')

const ApplicationError = require('../error/ApplicationError')
const errorHandler = require('../error/errorHandler')
const authServices = require('../services/auth')
const usersServices = require('../services/users')

const schemas = {
  loginPayload: superstruct.object({
    username: superstruct.string(),
    password: superstruct.string(),
  }),
}

const authControllers = {
  login: async (req, res, next) => {
    try {
      superstruct.assert(req.body, schemas.loginPayload)
      const targetUser = await usersServices.findUserByUsername(req.body.username)
      if (!targetUser) throw new ApplicationError(404, 'not_found', 'User not found by username')
      const isPasswordValid = authServices.passwords.comparePasswords(targetUser.hash, req.body.password)
      if (!isPasswordValid) throw new ApplicationError(400, 'bad_request', 'User password mismatch')
      const authToken = authServices.tokens.getTokenFromUser(targetUser)
      res.status(200).json({ token: authToken })
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
}

module.exports = authControllers
