class ApplicationError extends Error {
  constructor(statusCode, errorCode, errorDetails) {
    super(errorDetails)
    Object.setPrototypeOf(this, ApplicationError.prototype)
    this.errorCode = errorCode
    this.statusCode = statusCode
    this.errorDetails = errorDetails
  }
}

module.exports = ApplicationError
