const logReqInfo = (req, res, next) => {
  console.log(`received ${req.method} ${req.path} request`)
  return next()
}

module.exports = logReqInfo
