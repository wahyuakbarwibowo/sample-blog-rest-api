const logReqInfo = require('./logReqInfo')

const setupMiddlewares = (app) => {
  app.use(logReqInfo)
}

module.exports = setupMiddlewares
