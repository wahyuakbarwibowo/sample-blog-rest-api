const express = require('express')

const setupMiddlewares = require('./middlewares/setup')
const setupRoutes = require('./routes/setup')

const app = express()

setupMiddlewares(app)
setupRoutes(app)

module.exports = app
